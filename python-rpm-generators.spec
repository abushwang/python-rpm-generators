Summary:        Dependency generators for Python RPMs
Name:           python-rpm-generators
Version:        12
Release:        7%{?dist}
License:        GPLv2+
Url:            https://src.fedoraproject.org/rpms/python-rpm-generators
Source0:        https://raw.githubusercontent.com/rpm-software-management/rpm/102eab50b3d0d6546dfe082eac0ade21e6b3dbf1/COPYING
Source1:        python.attr
Source2:        pythondist.attr
Source3:        pythonname.attr
Source4:        pythondistdeps.py
Source5:        pythonbundles.py

BuildArch:      noarch

%description
This package is a dependency generators for Python RPMs.

%package -n python3-rpm-generators
Summary:        Dependency generators for Python RPMs
Requires:       python3-packaging
Requires:       rpm > 4.15.90-0
Requires:       python-srpm-macros >= 3.10-15

%description -n python3-rpm-generators
This package is a dependency generators for Python RPMs.

%prep
%autosetup -c -T
cp -a %{sources} .

%install
install -Dpm0644 -t %{buildroot}%{_fileattrsdir} *.attr
install -Dpm0755 -t %{buildroot}%{_rpmconfigdir} *.py

%files -n python3-rpm-generators
%license COPYING
%{_fileattrsdir}/python.attr
%{_fileattrsdir}/pythondist.attr
%{_fileattrsdir}/pythonname.attr
%{_rpmconfigdir}/pythondistdeps.py
%{_rpmconfigdir}/pythonbundles.py

%changelog
* Mon Apr 8 2024 Shuo Wang <abushwang@tencent.com> - 12-7
- remove unused import to pkg_resources

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 12-6
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 12-5
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 12-4
- Rebuilt for OpenCloudOS Stream 23

* Tue Feb 07 2023 cunshunxia <cunshunxia@tencent.com> - 12-3
- update the url.

* Wed Jan 11 2023 Miaojun Dong <zoedong@tencent.com> - 12-2
- remove vendor from macros.

* Fri Oct 28 2022 Miaojun Dong <zoedong@tencent.com> - 12-1
- Initial build
